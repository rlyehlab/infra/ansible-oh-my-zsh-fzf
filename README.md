## Descripción

Este ansible sirve para instalar zsh, oh my zsh y fzf para unx determinadx usuarix

Además configura un theme y plugins de zsh por defecto

Inlcuye el plugin `zsh-syntax-highlighting`

## Uso

Crear archivos `playbook.yml` y `hosts` a partir de los ejemplos:
```
cp playbook.yml.example playbook.yml
cp hosts.example hosts 
```

y configurar para su caso.

Ejecutar el playbook con:

`ansible-playbook playbook.yml -i hosts -euser=USUARIX_A_CONFIGURAR`
